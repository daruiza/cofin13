export const environment = {
  production: true,
  base: 'http://cofin.temposolutions.co',
  baseAPI: 'http://cofin.temposolutions.co/api',
  payu: 'https://api.payulatam.com/payments-api/4.0/service.cgi',
};
