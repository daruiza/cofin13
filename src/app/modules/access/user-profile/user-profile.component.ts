import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  @Input() pop: any;
  @Output() optionEvent = new EventEmitter<any>();
  
  constructor() { }

  ngOnInit(): void {
  }

}
