import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { IAlert } from 'src/app/models/IAlert';
import { IInputTextControl } from 'src/app/models/IInputControl';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../../../assets/css/access_login.css']
})
export class LoginComponent implements OnInit {

  @Input() pop: any;
  @Output() optionEvent = new EventEmitter<any>();

  loginForm: FormGroup;

  emailControl: IInputTextControl;
  passwordControl: IInputTextControl;

  onSubmitClick?: boolean;

  alert?: IAlert;

  constructor(
    public readonly router: Router,
    private readonly fb: FormBuilder,
    private readonly authService: AuthService,
    public readonly loadingService: LoadingService,
  ) {
    this.loginForm = this.fb.group({});
    this.emailControl = {
      type: 'email',
      name: 'email',
      label: 'Correo electrónico',
      placeholder: 'Correo electrónico',
      required: true,
    };
    this.passwordControl = {
      type: 'password',
      name: 'password',
      label: 'Contraseña',
      placeholder: 'Contraseña',
      required: true,
    };
  }

  ngOnInit(): void { }

  onSubmit(event: any) {
    if (this.authService.user && this.authService.checkLogin()) {
      console.log('ya tenemos usuario');
      return;
    }

    if (this.loginForm && this.loginForm.valid && !this.onSubmitClick) {
      this.onSubmitClick = true;
      this.authService.login(
        this.loginForm.value.email,
        this.loginForm.value.password
      ).subscribe({
        next: () => {
          // vamos por el usuario
          this.authService.getUser().subscribe(user => {
            this.onSubmitClick = false;
            this.loadingService.updatedSnackBehavior({
              message: 'Sessión inciada Correctamente',
              action: `Bienvebid@ ${user.name}!`,
              onAction: () => { this.router.navigate(['/home/welcome']); }
            });
            this.optionEvent.emit('home');
          })
        },
        error: (error) => {
          this.alert = {
            type: 'danger',
            title: error.statusText,
            message: error.error.message
          };
          this.onSubmitClick = false;
        }
      }
      );
    } else {
      this.validControl(this.loginForm);
    }
  }

  validControl(elementForm?: FormGroup): void {
    if (elementForm) {
      for (const inner in elementForm.controls) {
        if (Object.prototype.hasOwnProperty.call(elementForm.controls, inner)) {
          elementForm.get(inner)?.markAllAsTouched();
          elementForm.get(inner)?.updateValueAndValidity();
        }
      }
    }
    return;
  }

}
