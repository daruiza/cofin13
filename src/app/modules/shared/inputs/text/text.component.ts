import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, ValidatorFn, Validators, FormControl, FormBuilder } from '@angular/forms';
import { IFilterElement } from 'src/app/models/IFilterElement';
import { IInputTextControl } from 'src/app/models/IInputControl';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit, OnDestroy {

  @Input() form: FormGroup;
  @Input() control: IInputTextControl;
  @Output() valueChange = new EventEmitter<IFilterElement>();
  constructor(
    private readonly fb: FormBuilder,
  ) {
    this.form = this.fb.group({});
    this.control = {
      type: 'text', name: '', label: '', placeholder: ''
    }
  }

  // ngOnChanges(changes: SimpleChanges): void { }

  ngOnInit(): void {
    this.services();
    this.formCreateText();
    this.changesForm();
  }

  services() { }

  // Creación
  formCreateText(): void {
    const validatorsArrayText: ValidatorFn[] = [];
    if (this.control.required) {
      validatorsArrayText.push(Validators.required);
    }
    if (this.control.maxLength) {
      validatorsArrayText.push(Validators.maxLength(this.control.maxLength));
    }
    if (this.control.minLength) {
      validatorsArrayText.push(Validators.minLength(this.control.minLength));
    }
    if (this.control.pattern) {
      validatorsArrayText.push(Validators.pattern(this.control.pattern));
    }
    if (this.control.type === 'email') {
      validatorsArrayText.push(Validators.email);
    }

    this.form.addControl(
      this.control.name, new FormControl(null, {
        validators: validatorsArrayText,
        updateOn: this.onUpdateText()
      })
    );

    this.form.patchValue({ [this.control.name]: this.control.value });

    if (this.control.disable) { this.disableText(); }

    this.valueChange.emit({
      name: this.control.label,
      nameField: this.control.name,
      idField: undefined,
      value: this.control.value,
      init: true
    });
  }

  // Configuraciones

  onUpdateText(): 'change' | 'blur' | 'submit' {
    switch (this.control.updateOn) {
      case 'change':
        return 'change';
      case 'blur':
        return 'blur';
      case 'submit':
        return 'submit';
      default:
        return 'blur';
    }
  }

  // Eventos

  changesForm(): void {
    this.form.get(this.control.name)?.valueChanges.subscribe(
      res => {
        if (res) {
          this.valueChange.emit({
            name: this.control.label,
            nameField: this.control.name,
            idField: res,
            value: res,
            init: false
          });
        } else {
          this.valueChange.emit({
            name: this.control.label,
            nameField: this.control.name,
            idField: undefined,
            value: null,
            init: false
          });
        }
      }
    );
  }

  enableText(): void {
    if (this.form.get(this.control.name)) {
      this.form.get(this.control.name)?.enable();
    }
  }

  disableText(): void {
    if (this.form.get(this.control.name)) {
      this.form.get(this.control.name)?.disable();
    }
  }

  ngOnDestroy(): void {
    this.form.removeControl(this.control.name);
  }

  // Validaciones

  getValidClassText(control: boolean = false) {
    if (this.form.get(this.control.name)?.status !== 'DISABLED') {
      if ((
        this.form.get(this.control.name)?.touched ||
        this.form.get(this.control.name)?.dirty) &&
        !this.form.get(this.control.name)?.valid) {
        return 'is-invalid';
      } else {
        if (
          this.form.get(this.control.name)?.touched &&
          this.form.get(this.control.name)?.dirty &&
          this.form.get(this.control.name)?.valid) {
          return !control ? 'is-valid' : '';
        }
      }
    }
    return '';
  }

  errorsRequiredText() {
    if (this.form.get(this.control.name)?.errors) {
      return this.form.get(this.control.name)?.errors?.['required'];
    }
    return false;
  }

  errorsMaxLengthText() {
    if (this.form.get(this.control.name)?.errors) {
      return this.form.get(this.control.name)?.errors?.['maxlength'];
    }
    return false;
  }

  errorsMimLengthText() {
    if (this.form.get(this.control.name)?.errors) {
      return this.form.get(this.control.name)?.errors?.['minlength'];
    }
    return false;
  }

  errorsPatternText() {
    if (this.form.get(this.control.name)?.errors) {
      return this.form.get(this.control.name)?.errors?.['pattern'];
    }
    return false;
  }

  errorsEmail() {
    if (this.form.get(this.control.name)?.errors) {
      return this.form.get(this.control.name)?.errors?.['email'];
    }
    return false;
  }

}
