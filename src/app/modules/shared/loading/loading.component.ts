import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['../../../../assets/css/loading.css'],
})
export class LoadingComponent implements OnInit {

  @Input() class!: string;

  loadingSubject$!: Observable<boolean | undefined>;

  constructor(public readonly loadingService: LoadingService) {

  }
  ngOnInit(): void {
    this.loadingSubject$ = this.loadingService.getLoadingSubject();
  }

}
