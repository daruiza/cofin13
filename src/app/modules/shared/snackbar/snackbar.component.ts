import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { LoadingService } from 'src/app/services/loading.service';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ISnackModel } from 'src/app/models/ISnackModel';


@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss']
})
export class SnackbarComponent implements OnInit, OnDestroy {

  @Input() class!: string;

  snackRefSuscription!: Subscription;

  constructor(
    public readonly loadingService: LoadingService,
    private snackBar: MatSnackBar) { }

  ngOnDestroy(): void {
    if (this.snackRefSuscription) {
      this.snackRefSuscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.snackRefSuscription =
      this.loadingService.snackbar.subscribe((snack: ISnackModel) => {
        if (snack) {
          this.openSnackBar(snack.message, snack.action, snack.onAction );
        }
      });
  }

  // onAction es una funcion flecha
  openSnackBar(message: string, action: string, onAction: any = null) {
    let snackBarRef = this.snackBar.open(message, action, { duration: 9000 });
    if (onAction) {
      snackBarRef.onAction().subscribe(onAction)
    }
  }

}
