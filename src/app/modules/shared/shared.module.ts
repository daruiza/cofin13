import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SnackbarComponent } from './snackbar/snackbar.component';
import { AlertComponent } from './alert/alert.component';
import { LoadingComponent } from './loading/loading.component';
import { TextComponent } from './inputs/text/text.component';

import { NgSelectModule } from '@ng-select/ng-select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatRippleModule } from '@angular/material/core/ripple';

@NgModule({
  declarations: [
    SnackbarComponent,
    LoadingComponent,
    AlertComponent,
    TextComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSnackBarModule,
    MatCardModule,
    MatSlideToggleModule,
    MatButtonModule,
    // MatRippleModule,
  ],
  exports: [
    SnackbarComponent,
    LoadingComponent,
    AlertComponent,
    TextComponent,
    NgbModule,
    NgSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSnackBarModule,
    MatCardModule,
    MatSlideToggleModule,
    MatButtonModule,
    // MatRippleModule,
  ]
})
export class SharedModule { }
