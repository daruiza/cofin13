import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { AppService } from 'src/app/services/app.sercice';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['../../../../assets/css/header_toolbar.css']
})
export class ToolbarComponent implements OnInit {

  @ViewChild('pop') public popover?: NgbPopover;

  constructor(
    public readonly appService: AppService,
    public readonly authService: AuthService
  ) { }

  ngOnInit(): void { }

  public togglePopover(): void {

    if (this.popover?.isOpen()) {
      this.popover.close()
    } else {
      if (!this.popover?.isOpen()) this.popover?.open();
    }
  }

  optionEvent() {
    this.togglePopover();
  }

}
