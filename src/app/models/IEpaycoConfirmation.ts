export interface IEpaycoConfirmation {
    commerceId: number;
    factura?: string;
    ticketId?: number;
}
