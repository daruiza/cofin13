export interface IConfirmationParams {
    commerceId: number;
    identification: number;
}
