export interface IBankEpaycoRequest {
    bankCode?: string;
    bankName?: string;
}
