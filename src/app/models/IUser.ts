export interface IUser {
    id?: number;
    name?: string;
    surname?: string;
    email?: string;
    phone?: string;
    avatar?: string;
    password?: string;
    active?: string;
    rol_id?: number;
    acount_id?: number;
    commerce_id?: number;
    permits?: any[];
  }
