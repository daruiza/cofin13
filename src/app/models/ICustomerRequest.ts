export interface ICustomerRequest {
    identification?: number;
    commerce_id?: string;
    active?: number;
    limit?: number;
    sort?: number;
    page?: number;
}
