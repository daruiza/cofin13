export interface IInvoiceResponse {
    customer_id: number;
    dateEnd: string;
    dateStart: string;
    description: string;
    id: number;
    invoices_status_id: number;
    loop: number;
    loopDate: string;
    loopDay: number;
    name: string;
    number: string;
    updated_at: string;
    created_at: string;
}
