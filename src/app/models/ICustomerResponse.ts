export interface ICustomerResponse{
    acount_id: number;
    active: number;
    avatar: string;
    commerce_id: number;
    email: string;
    id: number;
    identification: number;
    document_type: string;
    name: string;
    phone: string;
    rol_id: number;
    surname: string;
    created_at: string;
    updated_at: string;
}
