import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

export interface IInputTextControl {
    type: 'email' | 'text' | 'password' | 'number';
    name: string;
    label: string;
    placeholder: string;
    tooltip?: string;
    required?: boolean;
    pattern?: RegExp;
    minLength?: number;
    maxLength?: number;
    min?: number;
    max?: number;
    updateOn?: string;
    disable?: boolean;
    value?: number | string;
    description?: number | string;
}
