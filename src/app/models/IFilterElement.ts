export interface IFilterElement {
    name: string; // nombre para presentar*
    fullName?: string; // autogenerado name y value
    nameField: string; // nombre para backend*
    idField?: number | string; // id para backend*
    value: any; // valor del filtro*
    init: boolean;
}
