export interface ICommerceRequest {
    id?: number;
    name?: string;
    description?: string;
    active?: number;
    limit?: number;
    sort?: number;
    page?: number;
}
