import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IUser } from 'src/app/models/IUser';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class AuthService {

    public url = `${environment.baseAPI}`;
    public baseUrl = `${environment.base}`;
    public httpHeaders: HttpHeaders;
    public nameToken: string;

    public user!: IUser | undefined;
    public userObservable: Observable<IUser | undefined>;
    private userBehavior = new BehaviorSubject<IUser | undefined>(undefined);

    constructor(
        private http: HttpClient,
        private readonly router: Router,
        // private readonly messagesAlertService: ModalAlertService,
    ) {
        this.nameToken = 'token_cofin';
        this.httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
        this.userObservable = this.userBehavior.asObservable();
    }

    public getNameToken(): string {
        return this.nameToken;
    }

    public setAccesToken(token: string): void {
        localStorage.setItem(this.nameToken, token);
    }

    public deleteToken(): void {
        localStorage.removeItem(this.nameToken);
    }

    public checkLogin(): boolean {
        return localStorage.getItem(this.nameToken) && localStorage.getItem(this.nameToken) !== 'undefined' ? true : false;
    }

    updatedUserBehavior(user: IUser | undefined) {
        this.userBehavior.next(user);
    }

    // Login
    public login(email: string, password: string): Observable<any> {
        const options = {
            headers: this.httpHeaders,
            params: {
                // email: `${email}`,
                // password: `${password}`,
            }
        };
        return this.http.post<any>(`${this.url}/auth/login`,
            {
                email: `${email}`,
                password: `${password}`
            },
            options)
            .pipe(
                tap(auth => {
                    this.setAccesToken(auth.access_token);
                }),
            );
    }

    public getUser(): Observable<any> {
        if (!this.user && this.checkLogin()) {
            const options = {
                headers: this.httpHeaders,
                params: {}
            };
            return this.http.get<any>(`${this.url}/auth/user`, options)
                .pipe(tap(resuser => {
                    this.user = resuser;
                    this.updatedUserBehavior(resuser);
                }));
        }
        return of({});
    }

    // Logout
    public logout(): Observable<any> {
        const options = {
            headers: this.httpHeaders,
            params: {}
        };
        return this.http.get<any>(`${this.url}/auth/logout`, options).pipe(
            tap(() => {
                this.user = undefined;
                this.updatedUserBehavior(undefined);
                this.deleteToken();
            })
        );
    }

    public logoutForce() {
        localStorage.removeItem(this.nameToken);
        this.user = undefined;
        this.router.navigate(['/']);
    }

}
