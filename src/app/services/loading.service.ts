import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { ISnackModel } from 'src/app/models/ISnackModel';

@Injectable({ providedIn: 'root' })
export class LoadingService {

  public loading: boolean;
  public loadingSubject = new BehaviorSubject<boolean | undefined>(undefined);

  public snackbar: Observable<any>;
  private snackBehavior = new BehaviorSubject<ISnackModel | undefined>(undefined);

  constructor() {
    this.loading = false;
    this.snackbar = this.snackBehavior.asObservable();
  }

  togleLoading() {
    this.loading = !this.loading;
  }

  updatedSnackBehavior(message: ISnackModel) {
    this.snackBehavior.next(message);
  }

  showTogleLoadingSubject() {
    this.loadingSubject.next(true);
  }

  hideTogleLoadingSubject() {
    this.loadingSubject.next(false);
  }

  getLoadingSubject() {
    return this.loadingSubject.asObservable();
  }
}
