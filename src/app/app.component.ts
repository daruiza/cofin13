import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from './services/app.sercice';
import { LoadingService } from './services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    public readonly router: Router,
    public readonly appService: AppService,
    public readonly loadingService: LoadingService,
  ) { }

}
